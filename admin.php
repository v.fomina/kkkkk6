<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||empty($_SERVER['PHP_AUTH_PW'])){
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
$user = 'u16354';
$pass = '8478228';
$db = new PDO(
  'mysql:host=localhost;dbname=u16354',
  $user,
  $pass,
  array(PDO::ATTR_PERSISTENT => true)
);
$stmt = $db->prepare('SELECT login, password FROM admin ');
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
  $login = strip_tags($row['login']);
  $password = strip_tags($row['password']);
}
if (
  $_SERVER['PHP_AUTH_USER'] != $login ||
  md5($_SERVER['PHP_AUTH_PW']) != $password) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}




?>
<head>
    <title>Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="main_adm.css">
    <link rel="stylesheet" type="text/css" href="css/style1.css" />
    <style type="text/css">
   TABLE {
    border-collapse: collapse; /* Убираем двойные линии между ячейками */
   }
   TD, TH {
    padding: 3px; /* Поля вокруг содержимого таблицы */
    border: 1px solid black; /* Параметры рамки */
   }
   TH {
    background: #b0e0e6; /* Цвет фона */
   }
  </style>
</head>

<body style="background-color: #666666;">
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <form class="login100-form validate-form" action="user_del.php" method="post">
          Вы успешно авторизовались и видите защищенные паролем данные.
          <div class="container-login100-form-btn">
            <input class="login100-form-btn" type="button" onclick="window.location= './';<?php ?>" value="Назад">
          </div>
          <br>
          <div class="container-login100-form-btn">
            <input class="login100-form-btn" type="button" onclick="window.location= './logout_adm.php';<?php ?>" value="Выход">
          </div>
          <?php
            $stmt = $db->prepare('SELECT id, fio, email, date, sex, amountOfLimbs, immortality, shapelessness, fly, biography, login FROM application5');
            $stmt->execute();
            print"<table>";
            print"<tr><td>id</td><td>fio</td><td>email.ru</td><td>date</td>
            <td>sex</td><td>amount of limbs</td><td>immortality</td><td>shapelessness</td><td>fly</td><td>biography</td><td>login</td><td>Delete</td></tr>";
            print"<tr>";
            while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
              for($i=0;$i<11;++$i) {
                echo "<td>";
                echo $row[$i];
                echo "</td>";
              }
              echo "<td>";
              print ("<div class=\"container-login100-form-btn\">
              <button class=\"login100-form-btn\" type=\"submit\" name=\"del_id\" value=\"");
              print $row["id"];
              print("\">X
              </button>
              </div>");
              echo "</td>";
              echo "</tr>";
            }
            echo "</table>";
          ?>
        </form>
        <div class="login100-more" style="background-image: url('david_bowie.jpg');" >
        </div>
      </div>
    </div>
  </div>
</body>


  