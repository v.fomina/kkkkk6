<?php

function gen_pass_and_log($length){
  $ac[0] = 'bcdfghjkmnpqrstvwxyz';
  $ac[1] = 'aeiou';
  $ac[2] = '123456789';
  //выберем цифирку для затравки
  $something = $ac[2][mt_rand(0, 8)];
  $length--;
  //для признака четности (чтобы выбрать нужный словарь)
  $n = 0;
  //пока не достигнем нужной длины, будем наращивать пароль        
  while ($length > 0) {
     $dict = $ac[$n++ % 2];
     $d_len = strlen($dict) - 1;
     $something = $dict[mt_rand(0, $d_len)] . $something . $dict[mt_rand(0, $d_len)];
     $length -= 2;
  }
  //пароль готов!
  
  return $something;
}

header('Content-Type: text/html; charset=UTF-8');
$ability_labels = ['immortality' =>'immortality', 'fly' => 'fly', 'shapelessness' => 'shapelessness'];
$ability_data = array_keys($ability_labels);
$user = 'u16354';
$pass = '8478228';
$authorized = 0;
$db = new PDO(
    'mysql:host=localhost;dbname=u16354',
    $user,
    $pass,
    array(PDO::ATTR_PERSISTENT => true)
  );
  


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куки, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';   
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['wfio'] = !empty($_COOKIE['wfio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['wemail'] = !empty($_COOKIE['wemail_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['wdate'] = !empty($_COOKIE['wdate_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['amountOfLimbs'] = !empty($_COOKIE['amountOfLimbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['wbiography'] = !empty($_COOKIE['wbiography_error']);
  $errors['ch'] = !empty($_COOKIE['ch_error']);
  
  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['wfio']) {
    setcookie('wfio_error', '', 100000);
    $messages[] = '<div class="error">Имя может состоять только из кириллицы и пробелов.</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните email.</div>';
  }
  if ($errors['wemail']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('wemail_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Введите корректно email. Ex: example@ex.com.</div>';
  }
  if ($errors['date']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('date_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['wdate']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('wdate_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните дату в формате ГГГГ-ММ-ДД.</div>';
  }
  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните пол.</div>';
  }
  if ($errors['amountOfLimbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('amountOfLimbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните число конечностей.</div>';
  }
  if ($errors['abilities']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('abilities_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните способности.</div>';
  }
  if ($errors['biography']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('biography_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['wbiography']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('wbiography_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Поле "биография" может содержать только кириллицу, знаки пунктуации и спец символы.</div>';
  }
  if ($errors['ch']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('ch_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Подтвердите согласие с политикой сайта.</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['amountOfLimbs'] = empty($_COOKIE['amountOfLimbs_value']) ? '' : $_COOKIE['amountOfLimbs_value'];
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['ch'] = empty($_COOKIE['ch_value']) ? '' : $_COOKIE['ch_value'];
  
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.

  if (empty(implode($errors)) && !empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
    // загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.

    $stmt = $db->prepare('SELECT name, email, date, sex, amountOfLimbs, immortality, shapelessness, fly, login FROM application5 WHERE id = ? ');
    $stmt->execute([$_SESSION['uid']]);
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
      $values['fio'] = strip_tags($row['name']);
      $values['email'] = strip_tags($row['email']);
      $values['date'] = strip_tags($row['date']);
      $values['sex'] = strip_tags($row['sex']);
      $values['amountOfLimbs'] = strip_tags($row['amountOfLimbs']);
      $values['immortality'] = strip_tags($row['immortality']);
      $values['shapelessness'] = strip_tags($row['shapelessness']);
      $values['fly'] = strip_tags($row['fly']);
    }
    $messages[] = '<div >Ваш логин: ' . $_SESSION['login'] . ' и id = ' . $_SESSION['uid'] . '</div>';
    $authorized = 1;
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
    setcookie('wfio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/(^[\w.]+)@([\w]+)\.([a-zA-Z]+$)/u', $_POST['email'])) {
    setcookie('wemail_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['date'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/', $_POST['date'])) {
    setcookie('wdate_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['sex'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['amountOfLimbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('amountOfLimbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('amountOfLimbs_value', $_POST['amountOfLimbs'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['abilities'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    $abilities = $_POST['abilities'];
    setcookie('abilities_value', implode(($_POST['abilities'])), time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/^[а-яА-Я ].+$/u', $_POST['biography'])) {
    setcookie('wbiography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['ch'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('ch_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('ch_value', $_POST['ch'], time() + 30 * 24 * 60 * 60);
  }
// *************
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('wfio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('wemail_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('wdate_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('amountOfLimbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('wbiography_error', '', 100000);
    setcookie('ch_error', '', 100000);
  }
  if (
    !empty($_COOKIE[session_name()]) &&
    session_start() && !empty($_SESSION['login']) ) {
    $ability_insert = [];
    foreach ($ability_data as $ability) {
        $ability_insert[$ability] = in_array($ability, $abilities) ? '+' : '-';
    }
    try {
        $stmt = $db->prepare("UPDATE application5 SET fio = ?, email = ?, date = ?, sex = ?, amountOfLimbs = ?, immortality = ?, shapelessness = ?, fly = ?, biography = ? WHERE id = ? ");
        $stmt->execute(array($_POST['fio'], $_POST['email'], $_POST['date'], $_POST['sex'], $_POST['amountOfLimbs'], $ability_insert['immortality'], $ability_insert['shapelessness'], $ability_insert['fly'], $_POST['biography'], $_SESSION['uid']));
    } 
    catch (PDOException $err) {
        print('Error : ' . $err->getMessage());
        exit();
    }
    
} 
    else {
        $found_same = false;
        do {
          $login = gen_pass_and_log(3);
          $pass = gen_pass_and_log(3);
    
          $stmt = $db->prepare('SELECT id, login FROM application5 WHERE login = ? AND password = ?');
          $stmt->execute([$login, md5($pass)]);
          while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
            $found_same = true;
          }
        } while  ($found_same != false);
    
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);


        // Сохранение в бд.
        $ability_insert = [];
        foreach ($ability_data as $ability) {
            $ability_insert[$ability] = in_array($ability, $abilities) ? '+' : '-';
        }
        try {
            $stmt = $db->prepare("INSERT INTO application5 SET fio = ?, email = ?, date = ?, sex = ?, amountOfLimbs = ?, immortality = ?, shapelessness = ?, fly = ?, biography = ?, login = ?, password = ?");
            $stmt -> execute(array($_POST['fio'], $_POST['email'], $_POST['date'], $_POST['sex'], $_POST['amountOfLimbs'], $ability_insert['immortality'], $ability_insert['fly'], $ability_insert['shapelessness'], $_POST['biography'], $login, md5($pass)));
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');
  // Делаем перенаправление.
  header('Location: index.php');
}
